/*Joshua Clark 991516472*/
/*This is the testing class*/
package passw;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidatorTest {

	@Test
	public void testPassValidLengthRegular() {
		boolean results = Validator.passValidLength("1234567890");
		assertTrue("Invalid length", results);
		
	}
	@Test
	public void testPassValidLengthException() {
		boolean results = Validator.passValidLength("");
		assertFalse("Invalid length", results);
	}
	@Test
	public void testPassValidLengthExceptionSpaces() {
		boolean results = Validator.passValidLength("         ");
		assertFalse("Invalid length", results);
	} 
	@Test
	public void testPassValidLengthBoundaryIn() {
		boolean results = Validator.passValidLength("12345678");
		assertTrue("Invalid length", results);
	}
//	@Test 
//	public void testPassValidLengthBoundaryOut() {
//		boolean results = Validator.passValidLength("1234567");
//		fail("Invalid length");
//	}
	@Test
	public void testPassValidNumRegular()
	{
		boolean results = Validator.passValidNum("Hello1234");
		assertTrue("Invalid number of digits", results);
	}
	@Test
	public void testPassValidNumException()
	{
		boolean results = Validator.passValidNum("Hellothere");
		assertFalse("Invalid number of digits", results);
	}
	@Test
	public void testPassValidNumBoundaryIn()
	{
		boolean results = Validator.passValidNum("Hellothere12");
		assertTrue("Invalid number of digits", results);
	}
	@Test
	public void testPassValidUpperRegular()
	{
		boolean results = Validator.passValidUpper("Despacito243");
		assertTrue("Invalid uppercase", results);
	}
	@Test
	public void testPassValidUpperException()
	{
		boolean results = Validator.passValidUpper("despacito243");
		assertFalse("Invalid uppercase", results);
	}
	@Test
	public void testPassValidUpperBoundaryIn()
	{
		boolean results = Validator.passValidUpper("DDDDDDDDD343");
		assertTrue("Invalid uppercase", results);
	}
}
