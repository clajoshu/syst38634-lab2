/*Joshua Clark 991516472*/
/*this class will act as a password validator, being created by TDD*/
package passw;

public class Validator {

	public static void main(String[] args) 
	{
		
	}
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT = 2;
	
	public static boolean passValidLength(String password)
	{
		return password.indexOf( " " )< 0 && password.trim().length() >= MIN_LENGTH;
	}
	public static boolean passValidNum(String password)
	{
		int count = 0;
		for (int i =0; i < password.length(); i++)
		{
			if (Character.isDigit(password.charAt(i)))
			{
				count++;
			}
		}
		if (count >= 2)
		{
			return true;
		}
		else {
			return false;
		}
	}
	public static boolean passValidUpper(String password)
	{
		int count = 0;
		for (int i =0; i < password.length(); i++)
		{
			if (Character.isUpperCase(password.charAt(i)))
			{
				count++;
			}
			
		}
		if (count >= 1)
			return true;
		else
			return false;
		
	}
}
